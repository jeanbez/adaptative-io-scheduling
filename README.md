# Adaptive Request Scheduling for the I/O Forwarding Layer using Reinforcement Learning

Jean Luca Bez (1, 3), Francieli Zanon Boito (2), Ramon Nou (3), Alberto Miranda (3), Toni Cortes (4,3), And Philippe O. A. Navaux (1)

1. Institute of Informatics, Federal University of Rio Grande do Sul (UFRGS), Porto Alegre, Brazil
2. LaBRI, Université de Bordeaux, Inria, CNRS, Bordeaux-INP, Bordeaux, France
3. Barcelona Supercomputing Center, Spain
4. Universitat Politécnica de Catalunya, Spain
